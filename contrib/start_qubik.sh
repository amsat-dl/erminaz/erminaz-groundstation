#!/bin/bash
if [ -z "$1" ]
  then
    echo "No QUBIK number provided"
    exit 1
fi

if [ -n "$2" ]
then
  SDR=$2
else
  SDR="pluto"
fi

if [ $(($1%2)) -eq 0 ]
then
	TX_f="435.34M"
else
	TX_f="435.24M"
fi

echo "TX Frequency ${TX_f}Hz"
echo "Starting flowgraph"
# USRP
if [ "$SDR" = "usrp" ]
then
  echo "Using USRP"
  ./qubik-comms-sw/test/flowgraphs/qubik_transceiver.py --gain-tx '78.0' --tx-freq $TX_f  --gain-rx '50' --soapy-rx-device 'driver=uhd' --antenna-rx 'RX2' --antenna-tx 'TX/RX' --bw=1e6 &
fi
# PlutoSDR
if [ "$SDR" = "pluto" ]
then
  echo "Using PlutoSDR"
  ./qubik-comms-sw/test/flowgraphs/qubik_transceiver.py --soapy-rx-device 'driver=plutosdr,hostname=192.168.20.1' --tx-freq $TX_f --samp-rate 1.024M --antenna-rx A_BALANCED --gain-rx 20 &
fi
GRC_PID=$!

echo "Starting listener"
./qubik-listener/qubik_listener.py --telemetry_url https://db-dev.satnogs.org/api/telemetry/

kill $GRC_PID
